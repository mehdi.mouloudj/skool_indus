import unittest
import pandas as pd
import nbformat
from nbconvert.preprocessors import ExecutePreprocessor
import os
import pathlib

BASEDIR = str(pathlib.Path().resolve().parent.absolute())
CORRECT_PATH = BASEDIR + '/tests/correct_y_output.csv'
TEST_PATH = BASEDIR + '/notebooks/y_test15.csv'
NOTEBOOK_PATH = BASEDIR + '/notebooks/titanic.ipynb'


class MyTestCase(unittest.TestCase):
    def test_notebook_result_is_not_changing(self):
        os.system('ipython kernel install --name "test_kernel" --user')

        nb_format_version = 4
        with open(NOTEBOOK_PATH) as f:
            titanic_notebook = nbformat.read(f, as_version=nb_format_version)
            execute_prepocessor = ExecutePreprocessor(timeout=600, kernel_name='test_kernel',
                                                      resources={'metadata': {'path': "../notebooks/"}})
            execute_prepocessor.preprocess(titanic_notebook)

        correct_df = pd.read_csv(CORRECT_PATH)
        test_df = pd.read_csv(TEST_PATH)
        pd.testing.assert_frame_equal(correct_df, test_df)


if __name__ == '__main__':
    unittest.main()
