import unittest
from src.feature_engineering import names, age_impute
import pandas as pd


class FeatureEngineeringTest(unittest.TestCase):
    def test_names(self):
        # GIVEN
        df1 = pd.DataFrame({'Name': ['Braund, Mr. Owen Harris', 'Heikkinen, Miss. Laina'], 'Age': [22, 23]})
        df2 = pd.DataFrame({'Name': ['Braund, Mr. Owen Harris', 'Heikkinen, Miss. Laina'], 'Age': [22, 23]})

        # WHEN
        train, test = names(df1, df2)

        # THEN
        df1_expected = pd.DataFrame({'Age': [22, 23],
                                    'Name_Len': [23, 22],
                                     'Name_Title': ['Mr.', 'Miss.'],
                                     })
        df2_expected = pd.DataFrame({'Age': [22, 23],
                                    'Name_Len': [23, 22],
                                     'Name_Title': ['Mr.', 'Miss.'],
                                     })
        pd.testing.assert_frame_equal(df1_expected, train)
        pd.testing.assert_frame_equal(df2_expected, test)

    def test_age_imput(self):
        # GIVEN
        df1 = pd.DataFrame({'Name': ['Braund, Mr. Owen Harris', 'Heikkinen, Miss. Laina'], 'Age': [22, 23]})
        df2 = pd.DataFrame({'Name': ['Braund, Mr. Owen Harris', 'Heikkinen, Miss. Laina'], 'Age': [22, 23]})

        # WHEN
        train, test = age_impute(df1, df2)

        # THEN
        df1_expected = pd.DataFrame({'Name': ['Braund, Mr. Owen Harris', 'Heikkinen, Miss. Laina'],
                                    'Age': [22, 23],
                                    'Age_Null_Flag': [0, 0],
                                     })
        df2_expected = pd.DataFrame({'Name': ['Braund, Mr. Owen Harris', 'Heikkinen, Miss. Laina'],
                                    'Age': [22, 23],
                                    'Age_Null_Flag': [0, 0],
                                     })
        pd.testing.assert_frame_equal(df1_expected, train)
        pd.testing.assert_frame_equal(df2_expected, test)



if __name__ == '__main__':
    unittest.main()
