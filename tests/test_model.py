import unittest
from unittest.mock import Mock

import pandas as pd

from src.ml_model import fit_model


class ModelMockTest(unittest.TestCase):
    def test_fit(self):

        # Given
        model = Mock()
        train = pd.DataFrame({'Survived': [0, 1], 'Name': ['Braund, Mr. Owen Harris', 'Heikkinen, Miss. Laina'], 'Age': [22, 23]})
        X = pd.DataFrame(train.iloc[:, 1:])
        y = pd.DataFrame(train, columns=['Survived'])
        # When
        fit_model(model, X, y)
        # Then
        model.fit.assert_called_once_with(X, y)
