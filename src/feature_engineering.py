import pandas as pd
from typing import Tuple


def names(train: pd.DataFrame, test: pd.DataFrame) -> Tuple[pd.DataFrame, pd.DataFrame]:
    train = train.copy()
    test = test.copy()
    for df in [train, test]:
        create_name_length_column(df)
        create_name_title_column(df)
        delete_name_column(df)
        
    return train, test


def create_name_title_column(dataframe: pd.DataFrame) -> None:  
    dataframe['Name_Title'] = dataframe['Name'].apply(lambda x: x.split(',')[1]).apply(lambda x: x.split()[0])


def create_name_length_column(dataframe: pd.DataFrame)-> None:
    dataframe['Name_Len'] = dataframe['Name'].apply(lambda x: len(x))

def delete_name_column(dataframe: pd.DataFrame)-> None:
    del dataframe['Name']


def age_impute(train: pd.DataFrame, test: pd.DataFrame):
    train = train.copy()
    test = test.copy()
    for i in [train, test]:
        i['Age_Null_Flag'] = i['Age'].apply(lambda x: 1 if pd.isnull(x) else 0)
        data = train.groupby(['Name_Title', 'Pclass'])['Age']
        i['Age'] = data.transform(lambda x: x.fillna(x.mean()))
    return train, test


