import unittest
import pandas as pd
import nbformat
from nbconvert.preprocessors import ExecutePreprocessor
import os
import pathlib


def fit_model(model, X, y) -> None:
    model.fit(X, y)
